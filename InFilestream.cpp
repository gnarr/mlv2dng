#include "InFilestream.h"
#include "Buffer.h"

InFilestream::InFilestream(std::string fileName)
	:file(fileName, std::ios::in | std::ios::binary)
{}

InFilestream::~InFilestream(void)
{
	file.close();
}

uint8_t  InFilestream::readUInt8()
{
	uint8_t data;
	file.read(reinterpret_cast<char*>(&data),sizeof(uint8_t));
	return data;
}

uint16_t InFilestream::readUInt16()
{
	uint16_t data;
	file.read(reinterpret_cast<char*>(&data),sizeof(uint16_t));
	return data;
}

uint32_t InFilestream::readUInt32()
{
	uint32_t data;
	file.read(reinterpret_cast<char*>(&data),sizeof(uint32_t));
	return data;
}

uint64_t InFilestream::readUInt64()
{
	uint64_t data;
	file.read(reinterpret_cast<char*>(&data),sizeof(uint64_t));
	return data;
}

void InFilestream::readBytes(void* object, uint32_t count)
{
	file.read(reinterpret_cast<char*>(object),count);
}

std::string InFilestream::readString(uint32_t length)
{
	Buffer<char> buffer(length+1);	
	file.read(&buffer,length);
	buffer[length] = 0;
	return std::string(&buffer);
}

bool InFilestream::readBool()
{
	bool data;
	file.read(reinterpret_cast<char*>(&data),1);
	return data;
}

void InFilestream::seek(std::streamoff bytes, std::ios_base::seekdir direction)
{
	file.seekg(bytes, direction);
}

std::streampos InFilestream::currentPosition()
{
	return file.tellg();
}

void InFilestream::open(std::string filename)
{
	file.open(filename, std::ios::in | std::ios::binary);
}

bool InFilestream::isGood() const
{
	return file.good();
}

bool InFilestream::eof() const
{
	return file.eof();
}
