#include "Info.h"


Info::Info(void)
	:Timestamped(0),
	infoString("NULL")
{}

Info::Info(InFilestream& inFile, uint32_t length)
	:Timestamped(inFile.readUInt64()),
	infoString(inFile.readString(length - 16))
{}

const std::string& Info::getInfoString() const
{
	return infoString;
}