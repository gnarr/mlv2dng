#ifndef ENTRYTYPE_H
#define ENTRYTYPE_H

#include <cstdint>

enum class Type : uint16_t
{
	Byte = 0x1,
	Ascii = 0x2,
	Short = 0x3,
	Long = 0x4,
	Rational = 0x5,
	SignedByte = 0x6,
	Undefined = 0x7,
	SingedShort = 0x8,
	SignedLong = 0x9,
	SignedRational = 0xA,
	Float = 0xB,
	Double = 0xC
};

#endif