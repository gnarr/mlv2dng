#ifndef IDENTITY_H
#define IDENTITY_H

#include <cstdint>
#include "InFilestream.h"
#include "Timestamped.h"

class Identity : public Timestamped
{
private:
    std::string	cameraName;    /* PROP (0x00000002), offset 0, length 32 */
    uint32_t    cameraModel;    /* PROP (0x00000002), offset 32, length 4 */
    std::string cameraSerial;    /* Camera serial number (if available) */
public:
	Identity (void);
	Identity (InFilestream& inFile);

	const std::string& getCameraName() const;
	uint32_t getCameraModel() const;
	const std::string& getCameraSerialHex() const;
	const std::string getCameraSerialDec() const;
};

#endif
