#ifndef FILEHEADER_H
#define FILEHEADER_H

#include <cstdint>
#include <fstream>
#include "InFilestream.h"

enum class VideoClass : uint16_t
{
	NONE = 0,
	RAW = 1,
	YUV = 2,
	JPEG = 3,
	H264 = 4
};

enum class AudioClass : uint16_t
{
	NONE = 0,
	WAV = 1
};

class FileHeader
{
private:
	std::string	versionString;		//	v2.0	null-terminated C-string of the exact revision of this format	
	uint64_t	fileGuid;			//		UID of the file (group) generated using hw counter, time of day and PRNG	file management information
	uint16_t	fileNum;			//	0	the ID within fileCount this file has (0 to fileCount-1)	
	uint16_t	fileCount;			//	1	how many files belong to this group (splitting or parallel)	
	uint32_t	fileFlags;			//	0	1=out-of-order data, 2=dropped frames, 4=single image mode, 8=stopped due to error	
	VideoClass	videoClass;			//	0	0=none, 1=RAW, 2=YUV, 3=JPEG, 4=H.264	quick access for viewers to know content
	AudioClass	audioClass;			//	0	0=none, 1=WAV	
	uint32_t	videoFrameCount;	//	0	number of video frames in this file. set to 0 on start, updated when finished.	
	uint32_t	audioFrameCount;	//	0	number of audio frames in this file. set to 0 on start, updated when finished.	
	uint32_t	sourceFpsNom;		//	25000	configured fps in 1/s multiplied by sourceFpsDenom	
	uint32_t	sourceFpsDenom;		//	1000	denominator for fps. usually set to 1000, but may be 1001 for NTSC	
public:
	FileHeader(InFilestream& inFile);

	const std::string& getVersion() const;
	uint64_t getFileGuid() const;
	uint16_t getFileNumber() const;
	uint16_t getFileCount() const;
	uint32_t getFileFlags() const;
	VideoClass getVideoClass() const;
	AudioClass getAudioClass() const;
	uint32_t getVideoFrameCount() const;
	uint32_t getAudioFrameCount() const;
	uint32_t getSourceFpsNominator() const;
	uint32_t getSourceFpsDenominator() const;
};

#endif