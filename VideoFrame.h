#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include <cstdint>
#include "InFilestream.h"
#include "Buffer.h"
#include "Timestamped.h"

#ifdef _MSC_VER
#define PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#elif defined(__GNUC__)
#define PACK( __Declaration__ ) __Declaration__ __attribute__((__packed__))
#endif

/* group 8 pixels in 16 bytes to simplify decoding */
PACK(
struct Raw16BytePixblock
{
	uint16_t b;
	uint16_t a;
	uint16_t d;
	uint16_t c;
	uint16_t f;
	uint16_t e;
	uint16_t h;
	uint16_t g;
});

/* group 8 pixels in 14 bytes to simplify decoding */
PACK(
struct Raw14BytePixblock
{
	uint16_t b_hi: 2;
    uint16_t a: 14;     // even lines: red; odd lines: green
    uint16_t c_hi: 4;
    uint16_t b_lo: 12;
    uint16_t d_hi: 6;
    uint16_t c_lo: 10;
    uint16_t e_hi: 8;
    uint16_t d_lo: 8;
    uint16_t f_hi: 10;
    uint16_t e_lo: 6;
    uint16_t g_hi: 12;
    uint16_t f_lo: 4;
    uint16_t h: 14;     // even lines: green; odd lines: blue
    uint16_t g_lo: 2;
});

/* group 8 pixels in 12 bytes to simplify decoding */
PACK(
struct Raw12BytePixblock
{
    uint16_t b_hi:	4;
	uint16_t a:		12;
	uint16_t c_hi:	8;
	uint16_t b_lo:	8;
	uint16_t d:		12;
	uint16_t c_lo:	4;
	uint16_t f_hi:	4;
	uint16_t e:		12;
	uint16_t g_hi:	8;
	uint16_t f_lo:	8;
	uint16_t h:		12;
	uint16_t g_lo:	4;
});

/* group 8 pixels in 10 bytes to simplify decoding */
PACK(
struct Raw10BytePixblock
{
	uint16_t b_hi:	6;
	uint16_t a:		10;
	uint16_t d_hi:	2;
	uint16_t c:		10;
	uint16_t b_lo:	4;
	uint16_t e_hi:	8;
	uint16_t d_lo:	8;
	uint16_t g_hi:	4;
	uint16_t f:		10;
	uint16_t e_lo:	2;
	uint16_t h:		10;
	uint16_t g_lo:	6;
});

/* group 8 pixels in 8 bytes to simplify decoding */
PACK(
struct Raw8BytePixblock
{
	uint16_t b: 8;
	uint16_t a: 8;
	uint16_t d: 8;
	uint16_t c: 8;
	uint16_t f: 8;
	uint16_t e: 8;
	uint16_t h: 8;
	uint16_t g: 8;
});

class VideoFrame : public Timestamped
{
private:
	uint32_t	frameNumber; //		unique video frame number
	uint16_t	cropPosX; //		specifies from which sensor row/col the video frame was copied (8x2 blocks)
	uint16_t	cropPosY; //		(can be used to process dead/hot pixels)
	uint16_t	panPosX; //		specifies the panning offset which is cropPos, but with higher resolution (1x1 blocks)
	uint16_t	panPosY; //		(it's the frame area from sensor the user wants to see)
	uint32_t	frameSpace; //		size of dummy data before frameData starts, necessary for EDMAC alignment
	uint32_t	frameSize;
	mutable Buffer<uint8_t> frameData;
public:
	typedef uint32_t (VideoFrame::*GetRawPixelFunction)(uint32_t,uint32_t,uint32_t) const;

	VideoFrame(InFilestream& inFile, uint32_t size);

	GetRawPixelFunction getRawPixel;

	uint32_t get16bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const;
	uint32_t get14bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const;
	uint32_t get12bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const;
	uint32_t get10bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const;
	uint32_t get8bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const;

	void setPixelBitDepth(uint32_t bits);

	void reverseByteOrder() const;

	uint32_t getFrameNumber() const;
	uint16_t getCropPositionX() const;
	uint16_t getCropPositionY() const;
	uint16_t getPanPositionX() const;
	uint16_t getPanPositionY() const;
	uint32_t getFrameSpace() const;
	uint32_t getFrameSize() const;
	uint8_t*& getFrameData() const;
};

#endif
