#include "MagicLanternVideo.h"
#include <exception>
#include <thread>
#include <iostream>

#include "Mlv2dng.h"
#include "HeaderNotFoundException.h"

MagicLanternVideo::MagicLanternVideo(const std::string& inputFile, const std::string& outPrefix)
	:fileHeader(NULL)
{
	gatherInfo(inputFile);
	processFrames(inputFile, outPrefix);
}

void MagicLanternVideo::gatherInfo(std::string inputFile)
{
	for(uint32_t i = 0; i < 100; ++i)
	{
		InFilestream fileStream(inputFile);
		if(!fileStream.isGood())
		{
			break;
		}
		for(BlockType current = static_cast<BlockType>(fileStream.readUInt32()); !fileStream.eof(); current = static_cast<BlockType>(fileStream.readUInt32()))
		{
			uint32_t blockSize = fileStream.readUInt32();
			std::streamoff postitionBeforeReading = fileStream.currentPosition();
			switch(current)
			{
			case BlockType::FileHeader:
				if(fileHeader != NULL)
				{
					delete fileHeader;
				}
				fileHeader = new FileHeader(fileStream);
				break;
			case BlockType::RawInfo:
				rawInfos.add(RawInfo(fileStream));
				break;
			case BlockType::WavInfo:
				wavInfos.add(WavInfo(fileStream));
				break;
			case BlockType::ExposureInfo:
				exposureInfos.add(ExposureInfo(fileStream));
				break;
			case BlockType::LensInfo:
				lensInfos.add(LensInfo(fileStream));
				break;
			case BlockType::RealTimeClock:
				realTimeClocks.add(RealTimeClock(fileStream));
				break;
			case BlockType::Info:
				infos.add(Info(fileStream, blockSize));
				break;
			case BlockType::Idendity:
				identities.add(Identity(fileStream));
				break;
			case BlockType::WhiteBalance:
				whiteBalanceInfos.add(WhiteBalanceInfo(fileStream));
				break;
			case BlockType::Null: // the null seek is done below the switch statement
				break;
			case BlockType::VideoFrame: // we need to skip over frames while gathering info
				break;
			default:
				std::cout << HeaderNotFoundException(current).what() << std::endl;
			}

			// To find mismatch between data read and the files actual blocksize we do the following:
			std::streamoff headerReadMismatch = (blockSize - 8) - (fileStream.currentPosition() - postitionBeforeReading);
			// we the seek from the current position to correct the mismatch.
			fileStream.seek(headerReadMismatch, std::ios_base::cur);
		}

		char postfix[3];
		sprintf(postfix,"%02u",i);
		inputFile.replace(inputFile.size()-2,2,postfix);
	}
}

void MagicLanternVideo::processFrames(std::string inputFile, const std::string& outPrefix)
{
	for(uint32_t i = 0; i < 100; ++i)
	{
		InFilestream fileStream(inputFile);
		if(!fileStream.isGood())
		{
			break;
		}
		for(BlockType current = static_cast<BlockType>(fileStream.readUInt32()); !fileStream.eof(); current = static_cast<BlockType>(fileStream.readUInt32()))
		{
			uint32_t blockSize = fileStream.readUInt32();
			std::streamoff postitionBeforeReading = fileStream.currentPosition();
			switch(current)
			{
			case BlockType::VideoFrame:
				{
					VideoFrame frame(fileStream, blockSize);
					Buffer<char> filename(outPrefix.size()+100);
					char* filenameP = &filename;
					sprintf(filenameP,"%s%06u.dng",outPrefix.c_str(), frame.getFrameNumber());
					Mlv2dng(filenameP, frame, *this);
				}
				break;
			default:
				break;
			}

			// To find mismatch between data read and the files actual blocksize we do the following:
			std::streamoff headerReadMismatch = (blockSize - 8) - (fileStream.currentPosition() - postitionBeforeReading);
			// we the seek from the current position to correct the mismatch.
			fileStream.seek(headerReadMismatch, std::ios_base::cur);
		}

		char postfix[3];
		sprintf(postfix,"%02u",i);
		inputFile.replace(inputFile.size()-2,2,postfix);
	}
}

MagicLanternVideo::~MagicLanternVideo(void)
{
	delete fileHeader;
}

const FileHeader& MagicLanternVideo::getFileHeader() const
{
	return *fileHeader;
}

const RawInfo& MagicLanternVideo::getRawInfo(uint64_t timestamp) const
{
	return rawInfos.getClosestLower(timestamp);
}

const WavInfo& MagicLanternVideo::getWavInfo(uint64_t timestamp) const
{
	return wavInfos.getClosestLower(timestamp);
}

const ExposureInfo& MagicLanternVideo::getExposureInfo(uint64_t timestamp) const
{
	return exposureInfos.getClosestLower(timestamp);
}

const LensInfo& MagicLanternVideo::getLensInfo(uint64_t timestamp) const
{
	return lensInfos.getClosestLower(timestamp);
}

const RealTimeClock& MagicLanternVideo::getRealTimeClock(uint64_t timestamp) const
{
	return realTimeClocks.getClosestLower(timestamp);
}

const Info& MagicLanternVideo::getInfo(uint64_t timestamp) const
{
	return infos.getClosestLower(timestamp);
}

const Identity& MagicLanternVideo::getIdentity(uint64_t timestamp) const
{
	return identities.getClosestLower(timestamp);
}

const WhiteBalanceInfo& MagicLanternVideo::getWhiteBalanceInfo(uint64_t timestamp) const
{
	return whiteBalanceInfos.getClosestLower(timestamp);
}
