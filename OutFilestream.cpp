#include "OutFilestream.h"


OutFilestream::OutFilestream(std::string fileName)
	:file(fileName, std::ios::out | std::ios::binary)
{}

void OutFilestream::write(const void* var, uint64_t size)
{
	file.write(reinterpret_cast<const char*>(var),size);
}

void OutFilestream::writeValue(uint32_t val, uint64_t size)
{
    write(&val,size);
}

void OutFilestream::writeValueSigned(int32_t val, uint64_t size)
{
    write(&val,size);
}

void OutFilestream::writeValue(Tag val, uint64_t size)
{
    write(&val,size);
}

void OutFilestream::writeValue(Type val, uint64_t size)
{
    write(&val,size);
}

void OutFilestream::writeZeros(uint64_t size)
{
	for(uint64_t i = 0; i < size; ++i)
	{
		writeValue(0, 1);
	}
}

void OutFilestream::seek(std::streamoff bytes, std::ios_base::seekdir direction)
{
	file.seekp(bytes, direction);
}

std::streampos OutFilestream::currentPosition()
{
	return file.tellp();
}

OutFilestream::~OutFilestream(void)
{
	file.close();
}
