#ifndef BITDEPTHEXCEPTION_H
#define BITDEPTHEXCEPTION_H

#include "Exception.h"
#include "TiffTags.h"

class BitDepthException : public Exception
{
public:
	BitDepthException(uint32_t depth) noexcept
	{
		std::stringstream stream;
		stream <<  "Exception! Mlv does not support a bit depth of "
		 << depth << "\n";
		message = stream.str();
	}
    virtual ~BitDepthException(void) noexcept {}
};

#endif