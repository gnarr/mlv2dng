#ifndef DIGITALNEGATIVE_H
#define DIGITALNEGATIVE_H

#include <vector>
#include "ImageFileDirectory.h"
#include "InFilestream.h"

typedef std::vector<ImageFileDirectory> IFDContainer;

class DigitalNegative
{
private:
	IFDContainer imageFileDirectories;
public:
	DigitalNegative();
	DigitalNegative(std::string fileName);

	void addIfd(ImageFileDirectory ifd);
	void writeHeader(OutFilestream& outFile);
	std::vector<uint32_t> getIfdOffsets() const;

	friend class Mlv2dng;
};

#endif
