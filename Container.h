#ifndef CONTAINER_H
#define CONTAINER_H

#include <cstdint>
#include <vector>
#include <algorithm>
#include "Timestamped.h"

template <typename T>
class Container
{
private:
    mutable std::vector<Timestamped*> items;
	mutable bool isSorted;
public:
    Container()
		:isSorted(true)
	{}

    void add(T item)
    {
        items.push_back(new T(item));
		isSorted = false;
    }

    const T& getClosestLower(uint64_t timestamp) const
    {
		// this hopefully does not happen, but if it does we need to add a null item.
		if(items.empty())
		{
			items.push_back(new T());
		}
		if(!isSorted)
		{
			std::sort(items.begin(), items.end());
			isSorted = true;
		}
        return *dynamic_cast<T*>(find(timestamp,0,items.size()-1));
    }

    ~Container()
    {
        for(auto& item : items)
        {
            delete item;
        }
    }

private:
    Timestamped* find(uint64_t timestamp, uint32_t indexMin, uint32_t indexMax) const
    {
		if(indexMax <= indexMin)
        {
            if(items[indexMin]->getTimestamp() < timestamp)
            {
                return items[indexMin];
            }
            return items[(indexMin > 0?indexMin-1:0)];
        }
        uint32_t midpoint = (indexMin + indexMax) / 2;

        if(items[midpoint]->getTimestamp() < timestamp)
        {
            return find(timestamp,midpoint+1,indexMax);
        }
        else if(items[midpoint]->getTimestamp() > timestamp)
        {
            return find(timestamp,indexMin,midpoint-1);
        }
        return items[midpoint];
    }
};

#endif
