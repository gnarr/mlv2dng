#include "FileHeader.h"


FileHeader::FileHeader(InFilestream& inFile )
	:versionString(inFile.readString(8)),
	fileGuid(inFile.readUInt64()),
	fileNum(inFile.readUInt16()),
	fileCount(inFile.readUInt16()),
	fileFlags(inFile.readUInt32()),
	videoClass(static_cast<VideoClass>(inFile.readUInt16())),
	audioClass(static_cast<AudioClass>(inFile.readUInt16())),
	videoFrameCount(inFile.readUInt32()),
	audioFrameCount(inFile.readUInt32()),
	sourceFpsNom(inFile.readUInt32()),
	sourceFpsDenom(inFile.readUInt32())
{}

const std::string&  FileHeader::getVersion() const
{
	return versionString;
}

uint64_t FileHeader::getFileGuid() const
{
	return fileGuid;
}

uint16_t FileHeader::getFileNumber() const
{
	return fileNum;
}

uint16_t FileHeader::getFileCount() const
{
	return fileCount;
}

uint32_t FileHeader::getFileFlags() const
{
	return fileFlags;
}

VideoClass FileHeader::getVideoClass() const
{
	return videoClass;
}

AudioClass FileHeader::getAudioClass() const
{
	return audioClass;
}

uint32_t FileHeader::getVideoFrameCount() const
{
	return videoFrameCount;
}

uint32_t FileHeader::getAudioFrameCount() const
{
	return audioFrameCount;
}

uint32_t FileHeader::getSourceFpsNominator() const
{
	return sourceFpsNom;
}

uint32_t FileHeader::getSourceFpsDenominator() const
{
	return sourceFpsDenom;
}

