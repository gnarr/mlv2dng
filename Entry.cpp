#include "Entry.h"
#include <iostream>
#include "Buffer.h"

Entry::Entry(Tag tag, Type type, uint32_t count, uint32_t offset,const EntryData& data)
	:tag(tag), 
	type(type),
	count(count),
	offset(offset),
	data(data)
{}

Entry::Entry(Tag tag, Type type, uint32_t data)
	:tag(tag),
	type(type),
	count(1),
	offset(data)
{}

Entry::Entry(Tag tag, uint8_t data)
	:tag(tag),
	type(Type::Byte),
	count(1),
	offset(data)
{}

Entry::Entry(Tag tag, uint16_t data)
	:tag(tag),
	type(Type::Short),
	count(1),
	offset(data)
{}

Entry::Entry(Tag tag, uint32_t data)
	:tag(tag),
	type(Type::Long),
	count(1),
	offset(data)
{}

Entry::Entry(Tag tag, std::string data)
	:tag(tag), 
	type(Type::Ascii),
	count(data.size()+1),
	offset(0)
{
	if(getTypeSize()*count > 4)
	{
		const char* dataPointer = data.c_str();
		this->data.assign(dataPointer, dataPointer+(count*sizeof(data[0])));
	}
	else
	{
		offset = *reinterpret_cast<uint32_t*>(&data[0]);
		for(int i = count; i < 4; ++i)
		{
			uint8_t* offsetP = reinterpret_cast<uint8_t*>(&offset)+i;
			*offsetP = 0;
		}
	}
}

Entry::Entry(Tag tag, ByteArray data)
	:tag(tag),
	type(Type::Byte),
	count(data.size()),
	offset(0)
{
	if(getTypeSize()*count > 4)
	{
		const uint8_t* dataPointer = &data[0];
		this->data.assign(dataPointer, dataPointer+(data.size()*sizeof(data[0])));
	}
	else
	{
		offset = *reinterpret_cast<uint32_t*>(&data[0]);
	}
}

Entry::Entry(Tag tag, ShortArray data)
	:tag(tag),
	type(Type::Short),
	count(data.size()),
	offset(0)
{
	if(getTypeSize()*count > 4)
	{
		const uint8_t* dataPointer = reinterpret_cast<uint8_t*>(&data[0]);
		this->data.assign(dataPointer, dataPointer+(data.size()*sizeof(data[0])));
	}
	else
	{
		offset = *reinterpret_cast<uint32_t*>(&data[0]);
	}
}

Entry::Entry(Tag tag, LongArray data)
	:tag(tag),
	type(Type::Long),
	count(data.size()),
	offset(0)
{
	if(getTypeSize()*count > 4)
	{
		const uint8_t* dataPointer = reinterpret_cast<uint8_t*>(&data[0]);
		this->data.assign(dataPointer, dataPointer+(data.size()*sizeof(data[0])));
	}
	else
	{
		offset = *reinterpret_cast<uint32_t*>(&data[0]);
	}
}

Entry::Entry(Tag tag, Type type, LongArray data)
	:tag(tag),
	type(type),
	count(data.size()*4/getTypeSize()),
	offset(0)
{
	if(getTypeSize()*count > 4)
	{
		const uint8_t* dataPointer = reinterpret_cast<uint8_t*>(&data[0]);
		this->data.assign(dataPointer, dataPointer+(data.size()*sizeof(data[0])));
	}
	else
	{
		offset = *reinterpret_cast<uint32_t*>(&data[0]);
	}
}

Entry::Entry(InFilestream& file)
	:tag(static_cast<Tag>(file.readUInt16())),
	type(static_cast<Type>(file.readUInt16())),
	count(file.readUInt32()),
	offset(file.readUInt32())
{
	if(getTypeSize()*count > 4)
	{
		int32_t bytesToRead = getTypeSize()*count;
		Buffer<uint8_t> buffer(bytesToRead);

		std::streamoff currentPosition = file.currentPosition();
		file.seek(offset, std::ios_base::beg);
		file.readBytes(&buffer, bytesToRead);
		file.seek(currentPosition, std::ios_base::beg);

		data.assign(&buffer, &buffer+bytesToRead);
	}
}

Type Entry::BaseType() const
{
	return static_cast<Type>(static_cast<uint16_t>(type) & 0xFF);
}

int Entry::getTypeSize() const
{
    switch(BaseType())
    {
	case Type::Byte:
    case Type::SignedByte:
    case Type::Undefined:
    case Type::Ascii:		return 1; 
    case Type::Short:
    case Type::SingedShort:	return 2;
    case Type::Long:
    case Type::SignedLong:
    case Type::Float:		return 4;
    case Type::Rational:
    case Type::SignedRational:
    case Type::Double:		return 8;
    default:				return 0;
    }
}

Tag Entry::getTag() const
{
	return tag;
}

Type Entry::getType() const
{
	return type;
}

uint32_t Entry::getCount() const
{
	return count;
}

uint32_t Entry::getOffset() const
{
	return offset;
}

const EntryData& Entry::getData() const
{
	return data;
}

void Entry::setTag(Tag tag)
{
	this->tag = tag;
}

void Entry::setType(Type type)
{
	this->type = type;
}

void Entry::setCount(uint32_t count)
{
	this->count = count;
}

void Entry::setOffset(uint32_t offset)
{
	this->offset = offset;
}

void Entry::setData(const EntryData& data)
{
	this->data = data;
}

void Entry::setData(const std::vector<uint32_t>& data)
{
	uint32_t bytes = data.size()*sizeof(uint32_t);
	const uint8_t* ifdArrayPointer = reinterpret_cast<const uint8_t*>(&data[0]);

	this->data = EntryData(ifdArrayPointer, ifdArrayPointer+bytes);
}

void Entry::setData(const std::vector<uint16_t>& data)
{
	uint32_t bytes = data.size()*sizeof(uint16_t);
	const uint8_t* ifdArrayPointer = reinterpret_cast<const uint8_t*>(&data[0]);

	this->data = EntryData(ifdArrayPointer, ifdArrayPointer+bytes);
}

uint32_t Entry::dataSize() const
{
	return data.size() + (data.size()&1);
}


void Entry::write(OutFilestream& outFile, uint64_t& dataOffset) const
{
	outFile.writeValue(tag, sizeof(uint16_t));
	outFile.writeValue(type, sizeof(uint16_t));
	outFile.writeValue(count, sizeof(uint32_t));
	if(getTypeSize()*count <= 4)
	{
		outFile.writeValue(offset, sizeof(uint32_t));
	}
	else
	{
		outFile.writeValue(static_cast<uint32_t>(dataOffset), sizeof(uint32_t));

		uint64_t currentPosition = outFile.currentPosition();
		outFile.seek(dataOffset, std::ios_base::beg);
		outFile.write(const_cast<uint8_t*>(&data[0]), data.size());
		outFile.writeValue(0, data.size()&1);
		dataOffset += data.size() + (data.size()&1);
		outFile.seek(currentPosition, std::ios_base::beg);
	}
}

