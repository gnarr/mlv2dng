#ifndef TIMESTAMPED_H
#define TIMESTAMPED_H

#include <cstdint>

class Timestamped
{
protected:
    uint64_t timestamp;
    Timestamped(uint64_t timestamp);
public:
    virtual uint64_t getTimestamp() const;
	virtual ~Timestamped();
	virtual bool operator < (const Timestamped& rhs);
};

#endif // TIMESTAMPED_H
