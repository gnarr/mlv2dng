#include "Identity.h"
#include <cstdint>
#include <cstdlib>
#include <sstream>

#ifdef _MSC_VER
#define strtoull _STRTO_ULL 
#endif

Identity::Identity(void)
	:Timestamped(0),
	cameraName("NULL"),
	cameraModel(0),
	cameraSerial("NULL")
{}

Identity::Identity(InFilestream& inFile)
	:Timestamped(inFile.readUInt64()),
	cameraName(inFile.readString(32)),
	cameraModel(inFile.readUInt32()),
	cameraSerial(inFile.readString(32))
{}

const std::string& Identity::getCameraName() const
{
	return cameraName;
}

uint32_t Identity::getCameraModel() const
{
	return cameraModel;
}

const std::string& Identity::getCameraSerialHex() const
{
	return cameraSerial;
}

const std::string Identity::getCameraSerialDec() const
{
	if(!cameraSerial.empty())
	{
		std::stringstream decimal;
		uint64_t number = strtoull(cameraSerial.c_str(), NULL, 16);
		decimal << number;
		return decimal.str();
	}
	return cameraSerial;
}
