#include "ExposureInfo.h"
#include <cstdint>
#include <cmath>
#include "InFilestream.h"
#include "Math.h"

ExposureInfo::ExposureInfo(void)
	:Timestamped(0),
	isoMode(0),
	isoValue(0),
	isoAnalog(0),
	digitalGain(0),
	shutterValue(0)
{}

ExposureInfo::ExposureInfo(InFilestream& inFile)
	:Timestamped(inFile.readUInt64()),
	isoMode(inFile.readUInt32()),
	isoValue(inFile.readUInt32()),
	isoAnalog(inFile.readUInt32()),
	digitalGain(inFile.readUInt32()),
	shutterValue(inFile.readUInt64())
{}

uint32_t ExposureInfo::getIsoMode() const
{
	return isoMode;
}

uint32_t ExposureInfo::getIsoValue() const
{
	return isoValue;
}

uint32_t ExposureInfo::getIsoAnalog() const
{
	return isoAnalog;
}

uint32_t ExposureInfo::getDigitalGain() const
{
	return digitalGain;
}

double ExposureInfo::getShutterValue() const
{
	return shutterValue/1000000.0;
}

uint64_t ExposureInfo::getShutterValueMicroseconds() const
{
	return shutterValue;
}

double ExposureInfo::getShutterValueApex() const
{
	return (log( pow(2.0,getShutterValue()-1.0) ) / log( 2 )) + 1.0;
}
