#ifndef INFILESTREAM_H
#define INFILESTREAM_H

#include <fstream>
#include <string>
#include <cstdint>

class InFilestream
{
private:
	std::ifstream file;
public:
	InFilestream(std::string fileName);
	~InFilestream(void);

	uint8_t  readUInt8();
	uint16_t readUInt16();
	uint32_t readUInt32();
	uint64_t readUInt64();
	void readBytes(void* object, uint32_t count);
	std::string readString(uint32_t length);
	bool readBool();
	void seek(std::streamoff bytes, std::ios_base::seekdir direction);
	std::streampos currentPosition();
	void open(std::string filename);
	bool isGood() const;
	bool eof() const;
};

#endif