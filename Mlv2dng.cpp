#include "Mlv2dng.h"
#include <iostream>

#define Init(array) array, array+sizeof(array)/sizeof(array[0])

Mlv2dng::Mlv2dng(std::string filename, const VideoFrame& videoFrame, const MagicLanternVideo& mlv)
	:videoFrame(videoFrame),
	fileHeader(mlv.getFileHeader()),
	rawInfo(mlv.getRawInfo(videoFrame.getTimestamp())),
	wavInfo(mlv.getWavInfo(videoFrame.getTimestamp())),
	exposureInfo(mlv.getExposureInfo(videoFrame.getTimestamp())),
	lensInfo(mlv.getLensInfo(videoFrame.getTimestamp())),
	realTimeClock(mlv.getRealTimeClock(videoFrame.getTimestamp())),
	info(mlv.getInfo(videoFrame.getTimestamp())),
	identity(mlv.getIdentity(videoFrame.getTimestamp())),
	whiteBalanceInfo(mlv.getWhiteBalanceInfo(videoFrame.getTimestamp()))
{
	createPreviewImageIfd();
	createMainImageIfd();
	createExifIfd();
	setOffsets();

	OutFilestream file(filename);
	dng.writeHeader(file);
	writeThumbnail(file);
	videoFrame.reverseByteOrder();
	file.write(videoFrame.getFrameData(), videoFrame.getFrameSize());
	//std::cout << filename << std::endl;
	std::cout << ".";
}

void Mlv2dng::createPreviewImageIfd()
{
	uint16_t bitsPerSample[]		= {8,8,8};
	uint32_t analogBalance[]		= {1,1,1,1,1,1};
	uint32_t asShotNeutral[]		= {94727,100000,200000,100000,124800,100000};

	uint32_t baselineNoise[]		= {1,1};
	uint32_t baselineSharpness[]	= {4,3};
	uint32_t linearResponseLimit[]	= {1,1};
	uint32_t frameRate[]			= {fileHeader.getSourceFpsNominator(),fileHeader.getSourceFpsDenominator()};

	Entry arrayInit[] = {
		Entry(Tag::NewSubfileType,				static_cast<uint32_t>(1)),
		Entry(Tag::ImageWidth,					dngWidth),
		Entry(Tag::ImageLength,					dngHeight),
		Entry(Tag::BitsPerSample,				ShortArray(Init(bitsPerSample))),
		Entry(Tag::Compression,					static_cast<uint16_t>(1)),
		Entry(Tag::PhotometricInterpretation,	static_cast<uint16_t>(2)),
		Entry(Tag::ImageDescription,			info.getInfoString()),
		Entry(Tag::Make,						"Canon"),
		Entry(Tag::Model,						identity.getCameraName()),
		Entry(Tag::StripOffsets,				static_cast<uint32_t>(0)),	// changed in setOffsets() function
		Entry(Tag::Orientation,					static_cast<uint16_t>(1)),
		Entry(Tag::SamplesPerPixel,				static_cast<uint16_t>(3)),
		Entry(Tag::RowsPerStrip,				static_cast<uint16_t>(dngHeight)),
		Entry(Tag::StripByteCounts,				static_cast<uint32_t>(dngWidth*dngHeight*3)),
		Entry(Tag::PlanarConfiguration,  		static_cast<uint16_t>(1)),
		Entry(Tag::Software,  					"Magic Lantern Video " + fileHeader.getVersion()),
		Entry(Tag::DateTime, 					realTimeClock.toString(videoFrame.getTimestamp())),
		Entry(Tag::Artist,  					""),
		Entry(Tag::SubIFDs,  					static_cast<uint32_t>(0)),	// changed in setOffsets() function
		Entry(Tag::Copyright, 					""),
		Entry(Tag::ExifIFD, 					static_cast<uint32_t>(0)),	// changed in setOffsets() function
		Entry(Tag::TIFF_EPStandardID, 			Type::Byte,				4,	0x00000001),
		Entry(Tag::DNGVersion, 					Type::Byte,				4,	0x00000301),
		Entry(Tag::DNGBackwardVersion, 			Type::Byte,				4,	0x00000301),
		Entry(Tag::UniqueCameraModel, 			identity.getCameraName()),
		Entry(Tag::ColorMatrix1, 				Type::SignedRational,	LongArray(Init(rawInfo.colorMatrix))),
		Entry(Tag::AnalogBalance, 				Type::Rational,			LongArray(Init(analogBalance))),
		Entry(Tag::AsShotNeutral, 				Type::Rational,			LongArray(Init(asShotNeutral))),
		Entry(Tag::BaselineExposure, 			Type::SignedRational,	LongArray(Init(rawInfo.exposureBias))),
		Entry(Tag::BaselineNoise, 				Type::Rational,			LongArray(Init(baselineNoise))),
		Entry(Tag::BaselineSharpness, 			Type::Rational,			LongArray(Init(baselineSharpness))),
		Entry(Tag::LinearResponseLimit, 		Type::Rational,			LongArray(Init(linearResponseLimit))),
		Entry(Tag::CalibrationIlluminant1, 		static_cast<uint16_t>(17)),
		Entry(Tag::CalibrationIlluminant2, 		static_cast<uint16_t>(21)),
		Entry(Tag::FrameRate, 					Type::SignedRational,	LongArray(Init(frameRate))),
		Entry(Tag::CameraSerialNumber,			identity.getCameraSerialDec()),
		Entry(Tag::LensInfo,					lensInfo.getLensName()),
		Entry(Tag::ImageNumber,					videoFrame.getFrameNumber())
	};
	dng.addIfd(ImageFileDirectory(EntryContainer(arrayInit, arrayInit+(sizeof(arrayInit)/sizeof(Entry)))));
}

void Mlv2dng::createMainImageIfd()
{
	uint32_t horizontalResolution[]	= {180, 1};
	uint32_t verticalResolution[]	= {180, 1};
	uint32_t badpixelOpcode[] =
	{
		// *** all values must be in big endian order
		BigEndian(static_cast<uint32_t>(1)),              // Count = 1
		BigEndian(static_cast<uint32_t>(4)),              // FixBadPixelsConstant = 4
		BigEndian(static_cast<uint32_t>(0x01030000)),     // DNG version = 1.3.0.0
		BigEndian(static_cast<uint32_t>(1)),              // Flags = 1
		BigEndian(static_cast<uint32_t>(8)),              // Opcode length = 8 bytes
		BigEndian(static_cast<uint32_t>(0)),              // Constant = 0
		BigEndian(static_cast<uint32_t>(0)),              // CFAPattern (set in code below)
	};
	Entry arrayInit[] = {
		Entry(Tag::NewSubfileType,				static_cast<uint32_t>(0)),
		Entry(Tag::ImageWidth,					static_cast<uint32_t>(rawInfo.configuredWidth)),
		Entry(Tag::ImageLength,					static_cast<uint32_t>(rawInfo.configuredHeight)),
		Entry(Tag::BitsPerSample,				static_cast<uint16_t>(rawInfo.bitsPerPixel)),
		Entry(Tag::Compression,					static_cast<uint16_t>(1)),// Uncompressed
		Entry(Tag::PhotometricInterpretation,	static_cast<uint16_t>(0x8023)),// CFA
		Entry(Tag::StripOffsets,				static_cast<uint32_t>(0)),	// changed in setOffsets() function
		Entry(Tag::SamplesPerPixel,				static_cast<uint16_t>(1)),
		Entry(Tag::RowsPerStrip,				static_cast<uint16_t>(rawInfo.configuredHeight)),
		Entry(Tag::StripByteCounts,				videoFrame.getFrameSize()),// CHDK RAW size
		Entry(Tag::XResolution,					Type::Rational,		LongArray(Init(horizontalResolution))),
		Entry(Tag::YResolution,					Type::Rational,		LongArray(Init(verticalResolution))),
		Entry(Tag::PlanarConfiguration,			static_cast<uint16_t>(1)),
		Entry(Tag::ResolutionUnit,				static_cast<uint16_t>(2)), // inch
		Entry(Tag::CFARepeatPatternDim,			Type::Short,	2,	0x00020002),// Rows = 2, Cols = 2
		Entry(Tag::CFAPattern,					Type::Byte,		4,	rawInfo.cfaPattern),
		Entry(Tag::BlackLevel,					rawInfo.blackLevel),
		Entry(Tag::WhiteLevel,					rawInfo.whiteLevel),
		Entry(Tag::DefaultCropOrigin,			LongArray(Init(rawInfo.crop.origin))),
		Entry(Tag::DefaultCropSize,				LongArray(Init(rawInfo.crop.size))),
		Entry(Tag::ActiveArea,					LongArray(Init(rawInfo.dngActiveArea))),
		Entry(Tag::OpcodeList1,					Type::Undefined,	LongArray(Init(badpixelOpcode)))
	};
	dng.addIfd(ImageFileDirectory(EntryContainer(arrayInit, arrayInit+(sizeof(arrayInit)/sizeof(Entry)))));
}

void Mlv2dng::createExifIfd()
{
	uint32_t shutter[]		= {static_cast<uint32_t>(exposureInfo.getShutterValueMicroseconds()), 1000000};
	uint32_t aperture[]		= {lensInfo.getApertureTimes100(), 100};
	uint32_t apexShutter[]	= {static_cast<uint32_t>(exposureInfo.getShutterValueApex() * 1000000), 1000000};
	uint32_t apexAperture[] = {static_cast<uint32_t>(lensInfo.getApertureApex() * 1000000), 1000000};
	uint32_t focalLength[]	= {lensInfo.getFocalLength(), 1};

	uint32_t maxAperture[]	= { 0, 96 };

	DateTime frameTime = realTimeClock.asDateTime(videoFrame.getTimestamp());
	std::string dateTime,subsecTime;
	frameTime.toString(dateTime, subsecTime);

	Entry arrayInit[] = {
		Entry(Tag::ExposureTime,			Type::Rational,			LongArray(Init(shutter))),          // Shutter speed
		Entry(Tag::FNumber,					Type::Rational,			LongArray(Init(aperture))),        // Aperture
		Entry(Tag::ExposureProgram,			Type::Short,			0),								// ExposureProgram
		Entry(Tag::ISOSpeedRatings,			Type::Short,			exposureInfo.getIsoValue()),       // ISOSpeedRatings
		Entry(Tag::ExifVersion,				Type::Undefined,		4,  0x31323230),                // ExifVersion: 2.21
		Entry(Tag::DateTimeOriginal,		dateTime),         // DateTimeOriginal
		Entry(Tag::ShutterSpeedValue,		Type::SignedRational,	LongArray(Init(apexShutter))),     // ShutterSpeedValue (APEX units)
		Entry(Tag::ApertureValue,			Type::Rational,			LongArray(Init(apexAperture))),    // ApertureValue (APEX units)
		Entry(Tag::ExposureBiasValue,		Type::SignedRational,	LongArray(Init(rawInfo.exposureBias))),         // ExposureBias
		Entry(Tag::MaxApertureValue,		Type::Rational,			LongArray(Init(maxAperture))),           // MaxApertureValue
		Entry(Tag::MeteringMode,			Type::Short,			static_cast<uint16_t>(0)),                         // Metering mode
		Entry(Tag::Flash,					Type::Short,			static_cast<uint16_t>(0)),                         // Flash mode
		Entry(Tag::FocalLength,				Type::Rational,			LongArray(Init(focalLength))),     // FocalLength
		Entry(Tag::SubsecTime,				subsecTime),       // microseconds
		Entry(Tag::SubsecTimeOriginal,		subsecTime),       // microseconds
		Entry(Tag::FocalLengthIn35mmFilm,	Type::Short,			lensInfo.getFocalLength()),    // FocalLengthIn35mmFilm
		Entry(Tag::SubjectDistance,			Type::Short,			lensInfo.getFocalDistance()),
		Entry(Tag::GainControl,				Type::Long,				exposureInfo.getDigitalGain()),
		Entry(Tag::WhiteBalance,			Type::Short,			(whiteBalanceInfo.getMode()==0?0:1))
	};
	dng.addIfd(ImageFileDirectory(EntryContainer(arrayInit, arrayInit+(sizeof(arrayInit)/sizeof(Entry)))));
}

void Mlv2dng::setOffsets()
{
	std::vector<uint32_t> offsets = dng.getIfdOffsets();

	// end of header
	uint32_t endOfHeader = offsets.back(); offsets.pop_back();

	// exif offset is set seperatly
	uint32_t exifOffset = offsets.back(); offsets.pop_back();

	// change subIfds entry
	Entry& subIfds = dng.imageFileDirectories[0].editEntry(Tag::SubIFDs);
	subIfds = Entry(Tag::SubIFDs, offsets);

	// change exifIfd entry
	Entry& exifIfd = dng.imageFileDirectories[0].editEntry(Tag::ExifIFD);
	exifIfd.setOffset(exifOffset);

	// offset for thumbnail
	Entry& ifd0stripOffset = dng.imageFileDirectories[0].editEntry(Tag::StripOffsets);
	ifd0stripOffset.setOffset(endOfHeader);

	uint32_t previewImageSize = dng.imageFileDirectories[0].getEntryDataAsUint32(Tag::StripByteCounts);

	// offset for main image
	Entry& ifd1stripOffset = dng.imageFileDirectories[1].editEntry(Tag::StripOffsets);
	ifd1stripOffset.setOffset(endOfHeader + previewImageSize);
}

void Mlv2dng::writeThumbnail(OutFilestream& outFile)
{
	uint8_t gammma[256];

	// fill gamma buffer
	for (int32_t i=0; i<12; i++) gammma[i]=powerCalc(255, i, 255, 0.5, 1);
	for (int32_t i=12; i<64; i++) gammma[i]=powerCalc(255, i, 255, 0.4, 1);
	for (int32_t i=64; i<=255; i++) gammma[i]=powerCalc(255, i, 255, 0.25, 1);

	uint32_t bufferSize = dngWidth*dngHeight*3;
	Buffer<uint8_t> thumbnailBuffer(bufferSize);

    register uint8_t *buf = &thumbnailBuffer;
    register uint32_t shift = rawInfo.bitsPerPixel - 8;

	// set the bit depth of the raw data for selecting the right getRawPixel function
	const_cast<VideoFrame&>(videoFrame).setPixelBitDepth(rawInfo.bitsPerPixel);
	// get a pointer to the appropriate getRawPixel function used in the loops below
	VideoFrame::GetRawPixelFunction getRawPixel = videoFrame.getRawPixel;

    // The sensor bayer patterns are:
    //  0x02010100  0x01000201  0x01020001
    //      R G         G B         G R
    //      G B         R G         B G
    // for the second pattern yadj shifts the thumbnail row down one line
    // for the third pattern xadj shifts the thumbnail row accross one pixel
    // these make the patterns the same
    register uint32_t yadj = (rawInfo.cfaPattern == 0x01000201) ? 1 : 0;
    register uint32_t xadj = (rawInfo.cfaPattern == 0x01020001) ? 1 : 0;

	uint32_t pitch = rawInfo.configuredWidth/8*rawInfo.bitsPerPixel;
	for (register uint32_t i=0; i < dngHeight; i++)
	{
        for (register uint32_t j=0; j < dngWidth; j++)
        {
			register uint32_t x = ((rawInfo.activeArea.x1 + rawInfo.jpeg.x + (rawInfo.jpeg.width  * j) / dngWidth)  & 0xFFFFFFFE) + xadj;
			register uint32_t y = ((rawInfo.activeArea.y1 + rawInfo.jpeg.y + (rawInfo.jpeg.height * i) / dngHeight) & 0xFFFFFFFE) + yadj;

			*buf++ = gammma[((videoFrame).*(getRawPixel))(x,y,pitch)>>shift];           // red pixel
            *buf++ = gammma[6*(((videoFrame).*(getRawPixel))(x+1,y,pitch)>>shift)/10];  // green pixel
            *buf++ = gammma[((videoFrame).*(getRawPixel))(x+1,y+1,pitch)>>shift];       // blue pixel
        }
	}
	outFile.write(&thumbnailBuffer, bufferSize);
}
