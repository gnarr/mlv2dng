#ifndef WAVFORMAT_H
#define WAVFORMAT_H

#include <cstdint>
#include "InFilestream.h"
#include "Timestamped.h"

enum class WavFormat
{
	IntegerPCM = 1,
	ALAW = 2,
	MULAW = 3
};

class WavInfo : public Timestamped
{
private:
	WavFormat	format; //	1	1=Integer PCM, 6=alaw, 7=mulaw
	uint16_t	channels; //	2	audio channel count: 1=mono, 2=stereo
	uint32_t	samplingRate; //	44100	audio sampling rate in 1/s
	uint32_t	bytesPerSecond; //		audio data rate
	uint16_t	blockAlign; //		see RIFF WAV hdr description
	uint16_t	bitsPerSample; //	16	audio ADC resolution
public:
	WavInfo(void);
	WavInfo(InFilestream& inFile);

	WavFormat getFormat();
	uint16_t getChannels();
	uint32_t getSamplingRate();
	uint32_t getBytesPerSecond();
	uint16_t getBlockAlign();
	uint16_t getBitsPerSample();
};

#endif