#include "VideoFrame.h"
#include <iostream>
#include "BitDepthException.h"

VideoFrame::VideoFrame(InFilestream& inFile, uint32_t size)
	:Timestamped(inFile.readUInt64()),
	frameNumber(inFile.readUInt32()),
	cropPosX (inFile.readUInt16()),
	cropPosY(inFile.readUInt16()),
	panPosX(inFile.readUInt16()),
	panPosY(inFile.readUInt16()),
	frameSpace(inFile.readUInt32()),
	frameSize((size - (frameSpace + 32))),
	frameData(frameSize)
{
	inFile.seek(frameSpace, std::ios::cur);
	inFile.readBytes(&frameData, frameSize);
}

uint32_t VideoFrame::get16bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const
{
	struct Raw16BytePixblock * p = reinterpret_cast<struct Raw16BytePixblock*>( &frameData + y * pitch + (x/8)*14);
    switch (x%8) {
        case 0: return p->a;
        case 1: return p->b;
        case 2: return p->c;
        case 3: return p->d;
        case 4: return p->e;
        case 5: return p->f;
        case 6: return p->g;
        case 7: return p->h;
    }
    return p->a;
}

uint32_t VideoFrame::get14bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const
{
	struct Raw14BytePixblock * p = reinterpret_cast<struct Raw14BytePixblock*>( &frameData + y * pitch + (x/8)*14);
    switch (x%8) {
        case 0: return p->a;
        case 1: return p->b_lo | (p->b_hi << 12);
        case 2: return p->c_lo | (p->c_hi << 10);
        case 3: return p->d_lo | (p->d_hi << 8);
        case 4: return p->e_lo | (p->e_hi << 6);
        case 5: return p->f_lo | (p->f_hi << 4);
        case 6: return p->g_lo | (p->g_hi << 2);
        case 7: return p->h;
    }
    return p->a;
}

uint32_t VideoFrame::get12bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const
{
	struct Raw12BytePixblock * p = reinterpret_cast<struct Raw12BytePixblock*>( &frameData + y * pitch + (x/8)*12);
    switch (x%8) {
        case 0: return p->a;
        case 1: return p->b_lo | (p->b_hi << 8);
        case 2: return p->c_lo | (p->c_hi << 4);
        case 3: return p->d;
        case 4: return p->e;
        case 5: return p->f_lo | (p->f_hi << 8);
        case 6: return p->g_lo | (p->g_hi << 4);
        case 7: return p->h;
    }
    return p->a;
}

uint32_t VideoFrame::get10bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const
{
	struct Raw10BytePixblock * p = reinterpret_cast<struct Raw10BytePixblock*>( &frameData + y * pitch + (x/8)*10);
    switch (x%8) {
        case 0: return p->a;
        case 1: return p->b_lo | (p->b_hi << 4);
        case 2: return p->c;
        case 3: return p->d_lo | (p->d_hi << 8);
        case 4: return p->e_lo | (p->e_hi << 8);
        case 5: return p->f;
        case 6: return p->g_lo | (p->g_hi << 6);
        case 7: return p->h;
    }
    return p->a;
}


// TODO: This probably doesn't need to be this complex and Raw8BytePixblock is probalby not needed at all. Can someone make this simpler? 
uint32_t VideoFrame::get8bitRawPixel(uint32_t x, uint32_t y, uint32_t pitch) const
{
	struct Raw8BytePixblock * p = reinterpret_cast<struct Raw8BytePixblock*>( &frameData + y * pitch + x);
    switch (x%8) {
        case 0: return p->a;
        case 1: return p->b;
        case 2: return p->c;
        case 3: return p->d;
        case 4: return p->e;
        case 5: return p->f;
        case 6: return p->g;
        case 7: return p->h;
    }
    return p->a;
}

void VideoFrame::setPixelBitDepth(uint32_t bits)
{
	switch(bits)
	{
	case 8:
		getRawPixel = &VideoFrame::get8bitRawPixel;
		break;
	case 10:
		getRawPixel = &VideoFrame::get10bitRawPixel;
		break;
	case 12:
		getRawPixel = &VideoFrame::get12bitRawPixel;
		break;
	case 14:
		getRawPixel = &VideoFrame::get14bitRawPixel;
		break;
	case 16:
		getRawPixel = &VideoFrame::get16bitRawPixel;
		break;
	default:
		throw new BitDepthException(bits);
		break;
	}
}

void VideoFrame::reverseByteOrder() const
{
	uint8_t* buffer = &frameData;
	uint16_t* bufferAsShort = reinterpret_cast<uint16_t*>(buffer);
	for(uint32_t i = 0; i < frameSize/2; ++i)
	{
		uint16_t x = bufferAsShort[i];
		buffer[2*i+1] = static_cast<uint8_t>(x);
		buffer[2*i] = x >> 8;
	}
}

uint32_t VideoFrame::getFrameNumber() const
{
	return frameNumber;
}

uint16_t VideoFrame::getCropPositionX() const
{
	return cropPosX;
}

uint16_t VideoFrame::getCropPositionY() const
{
	return cropPosY;
}

uint16_t VideoFrame::getPanPositionX() const
{
	return panPosX;
}

uint16_t VideoFrame::getPanPositionY() const
{
	return panPosY;
}

uint32_t VideoFrame::getFrameSpace() const
{
	return frameSpace;
}

uint32_t VideoFrame::getFrameSize() const
{
	return frameSize;
}

uint8_t*& VideoFrame::getFrameData() const
{
	return &frameData;
}


