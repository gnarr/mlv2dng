#ifndef EXPOSUREINFO_H
#define EXPOSUREINFO_H

#include <cstdint>
#include "InFilestream.h"
#include "Timestamped.h"

class ExposureInfo : public Timestamped
{
private:
	uint32_t	isoMode; //	0	0=manual, 1=auto
	uint32_t	isoValue; //	6400	camera delivered ISO value
	uint32_t	isoAnalog; //	3200	camera delivered ISO value
	uint32_t	digitalGain; //	1	digital ISO gain
	uint64_t	shutterValue; //		exposure time in microseconds
public:
	ExposureInfo(void);
	ExposureInfo(InFilestream& inFile);

	uint32_t getIsoMode() const;
	uint32_t getIsoValue() const;
	uint32_t getIsoAnalog() const;
	uint32_t getDigitalGain() const;
	double getShutterValue() const;
	uint64_t getShutterValueMicroseconds() const;
	double getShutterValueApex() const;
};

#endif