#include "LensInfo.h"
#include "Math.h"
#include <cmath>

LensInfo::LensInfo(void)
	:Timestamped(0),
	focalLength(0),
	focalDist(0),
	aperture(0),
	stabilizerMode(0),
	autofocusMode(0),
	flags(0),
	lensID(0),
	lensName("NULL"),
	lensSerial("NULL")
{}

LensInfo::LensInfo(InFilestream& inFile)
	:Timestamped(inFile.readUInt64()),
	focalLength(inFile.readUInt16()),
	focalDist(inFile.readUInt16()),
	aperture(inFile.readUInt16()),
	stabilizerMode(inFile.readBool()),
	autofocusMode(inFile.readBool()),
	flags(inFile.readUInt32()),
	lensID(inFile.readUInt32()),
	lensName(inFile.readString(32)),
	lensSerial(inFile.readString(32))
{}

uint16_t LensInfo::getFocalLength() const
{
	return focalLength;
}

uint16_t LensInfo::getFocalDistance() const
{
	return focalDist;
}

double LensInfo::getAperture() const
{
	return aperture/100;
}

uint32_t LensInfo::getApertureTimes100() const
{
	return aperture;
}

double LensInfo::getApertureApex() const
{
	return log( pow(getAperture(),2.0) ) / log( 2 );
}

bool LensInfo::getStabilizerMode() const
{
	return stabilizerMode;
}

bool LensInfo::getAutofocusMode() const
{
	return autofocusMode;
}

uint32_t LensInfo::getFlags() const
{
	return flags;
}

uint32_t LensInfo::getLensID() const
{
	return lensID;
}

std::string LensInfo::getLensName() const
{
	return lensName;
}

std::string LensInfo::getLensSerial() const
{
	return lensSerial;
}

