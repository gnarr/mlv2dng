#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <string>
#include <sstream>
#include <iomanip>

#ifdef _MSC_VER
#define noexcept
#endif

class Exception :	public std::exception
{
protected:
	std::string message;
protected:
	Exception() noexcept
	{}
    virtual ~Exception(void) noexcept {}
public:
	virtual const char* what() const noexcept
	{
		return message.c_str();
	}
};

#endif
