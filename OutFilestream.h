#ifndef OUTFILESTREAM_H
#define OUTFILESTREAM_H

#include <fstream>
#include <cstdint>
#include "TiffTags.h"
#include "EntryType.h"

class OutFilestream
{
private:
	std::ofstream file;
public:
	OutFilestream(std::string fileName);

	void write(const void* var, uint64_t size);
	void writeValue(uint32_t val, uint64_t size);
	void writeValueSigned(int32_t val, uint64_t size);
	void writeValue(Tag val, uint64_t size);
	void writeValue(Type val, uint64_t size);
	void writeZeros(uint64_t size);
	void seek(std::streamoff bytes, std::ios_base::seekdir direction);
	std::streampos currentPosition();
	~OutFilestream(void);
};

#endif
