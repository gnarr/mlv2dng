#include "RealTimeClock.h"
#include <cstdio>

RealTimeClock::RealTimeClock(void)
	:Timestamped(0),
	second(0),
	minute(0),
	hour(0),
	dayOfMonth(0),
	month(0),
	year(0),
	dayOfWeek(0),
	dayOfYear(0),
	isDst(0),
	gmtOffset(0),
	timezone("")
{}

RealTimeClock::RealTimeClock(InFilestream& inFile)
	:Timestamped(inFile.readUInt64()),
	second(inFile.readUInt16()),
	minute(inFile.readUInt16()),
	hour(inFile.readUInt16()),
	dayOfMonth(inFile.readUInt16()),
	month(inFile.readUInt16()),
	year(inFile.readUInt16()),
	dayOfWeek(inFile.readUInt16()),
	dayOfYear(inFile.readUInt16()),
	isDst(inFile.readUInt16()),
	gmtOffset(inFile.readUInt16()),
	timezone(inFile.readString(8))
{}

uint16_t RealTimeClock::getSeconds() const
{
	return second;
}

uint16_t RealTimeClock::getMinutes() const
{
	return minute;
}

uint16_t RealTimeClock::getHours() const
{
	return hour;
}

uint16_t RealTimeClock::getDayOfMonth() const
{
	return dayOfMonth;
}

uint16_t RealTimeClock::getMonth() const
{
	return month;
}

uint16_t RealTimeClock::getYear() const
{
	return year;
}

uint16_t RealTimeClock::getDayOfWeek() const
{
	return dayOfWeek;
}

uint16_t RealTimeClock::getDayOfYear() const
{
	return dayOfYear;
}

uint16_t RealTimeClock::isDaylightSaving() const
{
	return isDst;
}

uint16_t RealTimeClock::getGmtOffset() const
{
	return gmtOffset;
}

std::string RealTimeClock::getTimezone() const
{
	return timezone;
}

DateTime RealTimeClock::asDateTime(uint64_t frameTimestamp) const
{
	uint64_t microsecond = (frameTimestamp - getTimestamp()) + (getTimestamp()%1000000);
	return DateTime(1900+year, 1+dayOfYear, hour, minute, second, microsecond);
}

std::string RealTimeClock::toString() const
{
	char timeString[20];
	sprintf (timeString, "%04u:%02u:%02u %02u:%02u:%02u", 1900+year, month+1, dayOfMonth, hour, minute, second);
	return std::string(timeString);
}

std::string RealTimeClock::toString(uint64_t frameTimestamp) const
{
	return asDateTime(frameTimestamp).toString();
}