#include "ImageFileDirectory.h"
#include "EntryNotFoundException.h"

ImageFileDirectory::ImageFileDirectory()
{}

ImageFileDirectory::ImageFileDirectory(const EntryContainer& entries)
	:entries(entries)
{}

ImageFileDirectory::ImageFileDirectory(InFilestream& file)
{
	uint32_t size(file.readUInt16());
	for(uint16_t i = 0; i < size; ++i)
	{
		entries.push_back(Entry(file));
	}
}

bool ImageFileDirectory::hasIfdTag(Tag tag) const
{
	for(auto& entry : entries)
	{
		if(entry.getTag() == tag)
		{
			return true;
		}
	}
	return false;
}

Entry& ImageFileDirectory::editEntry(Tag tag)
{
	for(auto& entry : entries)
	{
		if(entry.getTag() == tag)
		{
			return entry;
		}
	}
	throw new EntryNotFoundException(tag);
}

const Entry& ImageFileDirectory::getEntry(Tag tag) const
{
	for(auto& entry : entries)
	{
		if(entry.getTag() == tag)
		{
			return entry;
		}
	}
	throw new EntryNotFoundException(tag);
}

uint16_t ImageFileDirectory::getEntryDataAsUint16(Tag tag) const
{
	for(auto& entry : entries)
	{
		if(entry.getTag() == tag)
		{
			if(entry.getCount() * entry.getTypeSize() > 4)
			{
				return *reinterpret_cast<const uint16_t*>(&entry.getData()[0]);
			}
			else
			{
				return entry.getOffset();
			}
		}
	}
	return 0;
}

uint32_t ImageFileDirectory::getEntryDataAsUint32(Tag tag) const
{
	for(auto& entry : entries)
	{
		if(entry.getTag() == tag)
		{
			if(entry.getCount() * entry.getTypeSize() > 4)
			{
				return *reinterpret_cast<const uint32_t*>(&entry.getData()[0]);
			}
			else
			{
				return entry.getOffset();
			}
		}
	}
	return 0;
}

std::vector<uint32_t> ImageFileDirectory::getEntryDataAsUint32Vector(Tag tag) const
{
	std::vector<uint32_t> data;
	for(auto& entry : entries)
	{
		if(entry.getTag() == tag)
		{
			if(entry.getCount() * entry.getTypeSize() > 4)
			{
				for(uint32_t i = 0; i < entry.getData().size(); i += 4)
				{
					data.push_back(*reinterpret_cast<const uint32_t*>(&entry.getData()[i]));
				}
			}
			else
			{
				data.push_back(entry.getOffset());
			}
			return data;
		}
	}
	return data;
}

uint32_t ImageFileDirectory::size() const
{
	return 2 + (entries.size() * 12) + 4;
}

uint32_t ImageFileDirectory::dataSize() const
{
	uint32_t dataSize = 0;
	for(auto& entry : entries)
	{
		dataSize += entry.dataSize();
	}
	return dataSize;
}

std::vector<uint32_t> ImageFileDirectory::dataSizes() const
{
	std::vector<uint32_t> sizes;
	for(auto& entry : entries)
	{
		sizes.push_back(entry.dataSize());
	}
	return sizes;
}

void ImageFileDirectory::write(OutFilestream& outFile, uint64_t& dataOffset) const
{
	outFile.writeValue(entries.size(), sizeof(uint16_t));
	for(auto& entry : entries)
	{
		entry.write(outFile, dataOffset);
	}
	outFile.writeValue(0,4);
}

