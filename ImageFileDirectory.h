#ifndef IMAGEFILEDIRECTORY_H
#define IMAGEFILEDIRECTORY_H

#include <vector>
#include "InFilestream.h"
#include "Entry.h"

typedef std::vector<Entry> EntryContainer;

class ImageFileDirectory
{
private:
	EntryContainer entries;
public:
	ImageFileDirectory();
	ImageFileDirectory(const std::vector<Entry>& entries);
	ImageFileDirectory(InFilestream& file);

	bool hasIfdTag(Tag tag) const;
	Entry& editEntry(Tag tag);
	const Entry& getEntry(Tag tag) const;
	uint16_t getEntryDataAsUint16(Tag tag) const;
	uint32_t getEntryDataAsUint32(Tag tag) const;
	std::vector<uint32_t> getEntryDataAsUint32Vector(Tag tag) const;
	uint32_t size() const;
	uint32_t dataSize() const;
	std::vector<uint32_t> dataSizes() const;
	void write(OutFilestream& outFile, uint64_t& dataOffset) const;
};

#endif