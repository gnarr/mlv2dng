#ifndef LENSINFO_H
#define LENSINFO_H

#include <cstdint>
#include "InFilestream.h"
#include "Timestamped.h"

class LensInfo : public Timestamped
{
private:
    uint16_t    focalLength;    /* in mm */
    uint16_t    focalDist;		/* in mm (65535 = infinite) */
    uint16_t	aperture;		/* f-number * 100 */
    bool		stabilizerMode;	/* 0=off, 1=on, (is the new L mode relevant) */
    bool		autofocusMode;  /* 0=off, 1=on */
    uint32_t    flags;			/* 1=CA avail, 2=Vign avail, ... */
    uint32_t    lensID;			/* hexadecimal lens ID (delivered by properties?) */
    std::string lensName;   /* full lens string */
    std::string lensSerial;	/* full lens serial number */
public:
	LensInfo(void);
	LensInfo(InFilestream& inFile);

	uint16_t    getFocalLength() const;
	uint16_t    getFocalDistance() const;
	double		getAperture() const;
	uint32_t	getApertureTimes100() const;
	double		getApertureApex() const;
	bool		getStabilizerMode() const;
	bool		getAutofocusMode() const;
	uint32_t    getFlags() const;
	uint32_t    getLensID() const;
	std::string getLensName() const;
	std::string getLensSerial() const;
};

#endif