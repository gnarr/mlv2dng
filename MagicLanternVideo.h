#ifndef MAGICLANTERNVIDEO_H
#define MAGICLANTERNVIDEO_H

#include <cstdint>
#include "FileHeader.h"
#include "RawInfo.h"
#include "WavInfo.h"
#include "ExposureInfo.h"
#include "LensInfo.h"
#include "RealTimeClock.h"
#include "Info.h"
#include "Identity.h"
#include "VideoFrame.h"
#include "WhiteBalanceInfo.h"
#include "Container.h"

enum class BlockType : uint32_t
{
	FileHeader = 0x49564c4d,
	VideoFrame = 0x46444956,
	Audio = 0x46445541,
	RawInfo = 0x49574152,
	WavInfo = 0x73866587,
	ExposureInfo = 0x4f505845,
	LensInfo = 0x534e454c,
	RealTimeClock = 0x49435452,
	Idendity = 0x544e4449,
	XREF = 0x70698288,
	Info = 0x4f464e49,
	DualISOInfo = 0x79837368,
	Empty = 0x76768578,
	Marker = 0x75826577,
	OffsetCorrectionFrame = 0x83707079,
	Vignette = 0x78717386,
	WhiteBalance = 0x4c414257,
	ElectronicLevel = 0x4c564c45,
	Null = 0x4c4c554e
};

class MagicLanternVideo
{
private:
	FileHeader* fileHeader;
	Container<RawInfo> rawInfos;
	Container<WavInfo> wavInfos;
	Container<ExposureInfo> exposureInfos;
	Container<LensInfo> lensInfos;
	Container<RealTimeClock> realTimeClocks;
	Container<Info> infos;
	Container<Identity> identities;
	Container<WhiteBalanceInfo> whiteBalanceInfos;
public:
	MagicLanternVideo(const std::string& inputFile, const std::string& outPrefix);
	~MagicLanternVideo(void);

	const FileHeader& getFileHeader() const;
	const RawInfo& getRawInfo(uint64_t timestamp) const;
	const WavInfo& getWavInfo(uint64_t timestamp) const;
	const ExposureInfo& getExposureInfo(uint64_t timestamp) const;
	const LensInfo& getLensInfo(uint64_t timestamp) const;
	const RealTimeClock& getRealTimeClock(uint64_t timestamp) const;
	const Info& getInfo(uint64_t timestamp) const;
	const Identity& getIdentity(uint64_t timestamp) const;
	const WhiteBalanceInfo& getWhiteBalanceInfo(uint64_t timestamp) const;
private:
	void gatherInfo(std::string inputFile);
	void processFrames(std::string inputFile, const std::string& outPrefix);
};

#endif