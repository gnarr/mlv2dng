#ifndef REALTIMECLOCK_H
#define REALTIMECLOCK_H

#include <cstdint>
#include "InFilestream.h"
#include "Timestamped.h"
#include "DateTime.h"

class RealTimeClock : public Timestamped
{
private:
	uint16_t	second;		//		seconds (0-59)
	uint16_t	minute;		//		minute (0-59)
	uint16_t	hour;	//		hour (0-24)
	uint16_t	dayOfMonth;	//		day of month (1-31)
	uint16_t	month;		//		month (1-12)
	uint16_t	year;	//		year (since 1900)
	uint16_t	dayOfWeek;	//		day of week
	uint16_t	dayOfYear;	//		day of year
	uint16_t	isDst;	//		daylight saving
	uint16_t	gmtOffset;	//		GMT offset
	std::string	timezone;		//		time zone string
public:
	RealTimeClock(void);
	RealTimeClock(InFilestream& inFile);

	uint16_t getSeconds() const;
	uint16_t getMinutes() const;
	uint16_t getHours() const;
	uint16_t getDayOfMonth() const;
	uint16_t getMonth() const;
	uint16_t getYear() const;
	uint16_t getDayOfWeek() const;
	uint16_t getDayOfYear() const;
	uint16_t isDaylightSaving() const;
	uint16_t getGmtOffset() const;
	std::string getTimezone() const;
	DateTime asDateTime(uint64_t frameTimestamp) const;
	std::string toString() const;
	std::string toString(uint64_t frameTimestamp) const;
	bool isLeapYear() const;
	int64_t daysSinceZero() const;
};

#endif