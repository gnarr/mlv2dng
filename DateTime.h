#ifndef DATETIME_H
#define DATETIME_H

#include <cstdint>
#include <string>

class DateTime
{
private:
	uint64_t microseconds;
public:
	DateTime(uint64_t microseconds);
	DateTime(uint16_t year, uint16_t dayOfYear, uint16_t hour, uint16_t minute, uint16_t second, uint64_t microsecond);

	std::string toString() const;
	void toString(std::string& dateTime, std::string& microseconds) const;
	void toValues(uint16_t& year, uint16_t& dayOfYear, uint16_t& hour, uint16_t& minute, uint16_t& second, uint32_t& microsecond) const;
	void toValues(uint16_t& year, uint16_t& month, uint16_t& dayOfMonth, uint16_t& hour, uint16_t& minute, uint16_t& second, uint32_t& microsecond) const;

	DateTime operator + (uint64_t microseconds);
	DateTime operator + (DateTime rhs);
	DateTime operator - (uint64_t microseconds);
	DateTime operator - (DateTime rhs);
	bool isLeapYear() const;

private:
	static const uint64_t microsecInSec = 1000000;
	static const uint64_t microsecInMin = (microsecInSec * 60);
	static const uint64_t microsecInHour = (microsecInMin * 60);
	static const uint64_t microsecInDay = (microsecInHour * 24);
	static const uint8_t daysInMonths[12];
};

#endif
