#include "WavInfo.h"

WavInfo::WavInfo(void)
	:Timestamped(0),
	format(WavFormat::IntegerPCM),
	channels(0),
	samplingRate(0),
	bytesPerSecond(0),
	blockAlign(0),
	bitsPerSample(0)
{}

WavInfo::WavInfo(InFilestream& inFile)
	:Timestamped(inFile.readUInt64()),
	format(static_cast<WavFormat>(inFile.readUInt16())),
	channels(inFile.readUInt16()),
	samplingRate(inFile.readUInt32()),
	bytesPerSecond(inFile.readUInt32()),
	blockAlign(inFile.readUInt16()),
	bitsPerSample(inFile.readUInt16())
{}

WavFormat WavInfo::getFormat()
{
	return format;
}

uint16_t WavInfo::getChannels()
{
	return channels;
}

uint32_t WavInfo::getSamplingRate()
{
	return samplingRate;
}

uint32_t WavInfo::getBytesPerSecond()
{
	return bytesPerSecond;
}

uint16_t WavInfo::getBlockAlign()
{
	return blockAlign;
}

uint16_t WavInfo::getBitsPerSample()
{
	return bitsPerSample;
}

