#include "Timestamped.h"

Timestamped::Timestamped(uint64_t timestamp)
:timestamp(timestamp)
{}

uint64_t Timestamped::getTimestamp() const
{
    return timestamp;
}

Timestamped::~Timestamped()
{}

bool Timestamped::operator < (const Timestamped& rhs)
{
	return timestamp < rhs.timestamp;
}
