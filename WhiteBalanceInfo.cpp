#include "WhiteBalanceInfo.h"

WhiteBalanceInfo::WhiteBalanceInfo(void)
	:Timestamped(0),
	mode(0),
	kelvin(0),
	gainRed(0),
	gainGreen(0),
	gainBlue(0),
	shift(0),
	balance(0)
{}

WhiteBalanceInfo::WhiteBalanceInfo(InFilestream& inFile)
	:Timestamped(inFile.readUInt64()),
	mode(inFile.readUInt32()),
	kelvin(inFile.readUInt32()),
	gainRed(inFile.readUInt32()),
	gainGreen(inFile.readUInt32()),
	gainBlue(inFile.readUInt32()),
	shift(inFile.readUInt32()),
	balance(inFile.readUInt32())
{}

uint32_t WhiteBalanceInfo::getMode() const
{
	return mode;
}

uint32_t WhiteBalanceInfo::getKelvin() const
{
	return kelvin;
}

uint32_t WhiteBalanceInfo::getRedGain() const
{
	return gainRed;
}

uint32_t WhiteBalanceInfo::getGreenGain() const
{
	return gainGreen;
}

uint32_t WhiteBalanceInfo::getBlueGain() const
{
	return gainBlue;
}

uint32_t WhiteBalanceInfo::getShift() const
{
	return shift;
}

uint32_t WhiteBalanceInfo::getBalance() const
{
	return balance;
}

