#include <iostream>
#include <string>
#include <algorithm>

#include "MagicLanternVideo.h"
#include "DigitalNegative.h"
#include "OutFilestream.h"

using namespace std;

inline bool exists (const std::string& name)
{
    if (FILE *file = fopen(name.c_str(), "r"))
    {
        fclose(file);
        return true;
    }
    return false;
}

int main(int argc, char **argv)
{
	if(argc <= 1)
	{
		cout << "usage:\n\nmlv2dng.exe file.mlv [prefix]\n\n => will create prefix000000.dng, prefix000001.dng and so on.\n";
		return 0;
	}
	string filename(argv[1]);
	string filenameLower(filename.length(),'\0');
	transform(filename.begin(), filename.end(), filenameLower.begin(), ::tolower);

	if(!exists(filename))
	{
	cout << "File nout found!\n";
	return 0;
	}
	if(filenameLower.substr(filenameLower.length() -4) != ".mlv")
	{
		cout << "Can only convert .mlv files!\n";
cout << filenameLower.substr(filenameLower.length() -4) << endl;
		return 0;
	}
	string outPrefix(argc>2?argv[2]:"");
	try
	{
		MagicLanternVideo mlv(filename, outPrefix);
	}
	catch(exception* e)
	{
		cout << e->what() << endl;
	}

	return 0;
}
